<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Inicio | Chronnooss</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App css -->
    <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" id="light-style">
    <link href="assets/css/app-dark.min.css" rel="stylesheet" type="text/css" id="dark-style">

    <!-- Custom css -->
    <link href="assets_base/css/styles.css" rel="stylesheet" type="text/css">

</head>

<body class="loading"
    data-layout-config='{"leftSideBarTheme":"dark","layoutBoxed":false, "leftSidebarCondensed":false, "leftSidebarScrollable":false,"darkMode":false, "showRightSidebarOnStart": true}'>
    <!-- Begin page -->
    <div class="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <?php
        
            include "paginas/left-sidebar.php";
        
        ?>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <!-- Topbar Start -->
                <?php
                
                    include "paginas/topbar.php";
                
                ?>
                <!-- end Topbar -->

                <!-- Start Content-->
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Chronnooss</a></li>
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bibliotecas</a></li>
                                        <li class="breadcrumb-item active">Pokémon</li>
                                    </ol>
                                </div>
                                <h4 class="page-title">Biblioteca de Pokémon</h4>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                               
                            </div> <!-- end card -->
                        </div><!-- end col-->                        
                    </div>
                </div> <!-- container -->

            </div> <!-- content -->

            <!-- Footer Start -->
            <?php
            
                include "paginas/footer.php";
            
            ?>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->


    <!-- Right Sidebar -->
    <?php
    
        include "paginas/right-sidebar.php";
    
    ?>
    <!-- /End-bar -->

    <!-- Template -->
    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <!-- Plugins -->
    <!-- -- Rating -->
    <script src="assets/js/vendor/jquery.rateit.min.js"></script>
    <!-- Checkbox -->
    <script src="assets/js/vendor/jquery.dataTables.min.js"></script>
    <script src="assets/js/vendor/dataTables.bootstrap5.js"></script>
    <script src="assets/js/vendor/dataTables.responsive.min.js"></script>
    <script src="assets/js/vendor/responsive.bootstrap5.min.js"></script>
    <script src="assets/js/vendor/dataTables.checkboxes.min.js"></script>

    <!-- Jquery -->
    <script src="assets_base/js/plugins/jquery/jquery.min.js"></script>
    
    <!-- Ajax -->
    <script src="assets_base/js/ajax/ajax.js"></script>

    <!-- Custom -->
    <script src="assets_base/js/main.js"></script>
</body>

</html>