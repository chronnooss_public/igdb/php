<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Inicio | Chronnooss</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- App favicon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">

    <!-- App css -->
    <link href="assets/css/icons.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/app.min.css" rel="stylesheet" type="text/css" id="light-style">
    <link href="assets/css/app-dark.min.css" rel="stylesheet" type="text/css" id="dark-style">

    <!-- Custom css -->
    <link href="assets_base/css/styles.css" rel="stylesheet" type="text/css">

</head>

<body class="loading"
    data-layout-config='{"leftSideBarTheme":"dark","layoutBoxed":false, "leftSidebarCondensed":false, "leftSidebarScrollable":false,"darkMode":false, "showRightSidebarOnStart": true}'>
    <!-- Begin page -->
    <div class="wrapper">

        <!-- ========== Left Sidebar Start ========== -->
        <?php
        
            include "paginas/left-sidebar.php";
        
        ?>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start Page Content here -->
        <!-- ============================================================== -->

        <div class="content-page">
            <div class="content">
                <!-- Topbar Start -->
                <?php
                
                    include "paginas/topbar.php";
                
                ?>
                <!-- end Topbar -->

                <!-- Start Content-->
                <div class="container-fluid">

                    <!-- start page title -->
                    <div class="row">
                        <div class="col-12">
                            <div class="page-title-box">
                                <div class="page-title-right">
                                    <ol class="breadcrumb m-0">
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Chronnooss</a></li>
                                        <li class="breadcrumb-item"><a href="javascript: void(0);">Bibliotecas</a></li>
                                        <li class="breadcrumb-item active">Videojuegos</li>
                                    </ol>
                                </div>
                                <h4 class="page-title">Biblioteca de Videojuegos</h4>
                            </div>
                        </div>
                    </div>
                    <!-- end page title -->
                    
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">

                                    <h4 class="header-title">videojuegos</h4>

                                        <form id="formCatalogoVideojuegos">
                                            <div class="row g-2">
                                                <div class="mb-3 col-xs-12 col-md-4">
                                                    <label for="txtBusquedaJuego" class="form-label">Búsqueda</label>
                                                    <input type="text" class="form-control" id="txtBusquedaJuego"
                                                        placeholder="Ingresa lo que desees buscar..">
                                                </div>
                                                <div class="mb-3 col-xs-12 col-md-4">
                                                    <label for="selLimite" class="form-label">Límite</label>
                                                    <input class="form-control" id="selLimite" type="number" min="1" max="500"
                                                        value="1" name="number">
                                                </div>
                                                <div class="mb-3 col-xs-12 col-md-4">
                                                    <p class="mb-1 fw-bold text-muted">Plataforma</p>
                                                        <select class="form-control select2" id="selPlataformas" data-toggle="select2">
                                                            <option value="0">Todas las plataformas</option>
                                                            <optgroup label="Nintendo">
                                                                <option disabled>Consolas de mesa</option>
                                                                <option value="18">Nintendo</option>
                                                                <option value="19">Super Nintendo</option>
                                                                <option value="4">Nintendo 64</option>
                                                                <option value="21">Nintendo GameCube</option>
                                                                <option value="5">Nintendo Wii</option>
                                                                <option value="41">Nintendo Wii U</option>
                                                                <option value="130">Nintendo Switch</option>
                                                                <option disabled>Consolas portatiles</option>
                                                                <option value="33">Game Boy</option>
                                                                <option value="22">Game Boy Color</option>
                                                                <option value="24">Game Boy Advance</option>
                                                                <option value="20">Nintendo DS</option>
                                                                <option value="159">Nintendo DSi</option>
                                                                <option value="37">Nintendo 3DS</option>
                                                                <option value="137">Nintendo New 3ds</option>
                                                            </optgroup>
                                                            <optgroup label="Sony">
                                                                <option disabled>Consolas de mesa</option>
                                                                <option value="7">PlayStation 1</option>
                                                                <option value="8">PlayStation 2</option>
                                                                <option value="9">PlayStation 3</option>
                                                                <option value="48">PlayStation 4</option>
                                                                <option value="167">PlayStation 5</option>
                                                                <option disabled>Consolas portatiles</option>
                                                                <option value="38">PlayStation Portatil</option>
                                                                <option value="46">PlayStation Vita</option>
                                                            </optgroup>
                                                            <optgroup label="Microsoft">
                                                                <option disabled>Consolas de mesa</option>
                                                                <option value="11">Xbox</option>
                                                                <option value="12">Xbox 360</option>
                                                                <option value="49">Xbox One</option>
                                                                <option value="169">Xbox Series</option>
                                                            </optgroup>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>

                                            <button type="submit" class="btn btn-primary">Buscar</button>
                                        </form>
                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->
                        <div class="col-xs-12">
                            <div class="card">
                                <div class="card-body">            
                                        <div class="table-responsive">
                                            <table id="tblCatalogoVideojuegos" class="table table-hover table-centered mb-0">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                        <!-- <th>Id</th> -->
                                                        <th>Imagen</th>
                                                        <th>Nombre</th>
                                                        <th>Plataforma(s)</th>
                                                        <th>Genero(s)</th>
                                                        <th>Modo(s) de juego</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="tblCatalogoVideojuegosBody">
                                                    
                                                </tbody>
                                            </table>
                                        </div> <!-- end table-responsive-->
                                </div> <!-- end card body-->
                            </div> <!-- end card -->
                        </div><!-- end col-->

                        <div class="row">
                            <div class="col-xs-12 col-md-4">
                                <div class="card">
                                    <div class="card-body">            
                                            <h2>Nintendo</h2>
                                            <div>
                                                <table id="tblListaNintendo" class="table table-hover table-centered mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Plataforma</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tblListaNintendoBody">

                                                    </tbody>
                                                </table>
                                            </div>
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                            <div class="col-xs-12 col-md-4">
                                <div class="card">
                                    <div class="card-body">            
                                            <h2>Sony</h2>
                                            <div>
                                                <table id="tblListaSony" class="table table-hover table-centered mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Plataforma</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tblListaSonyBody">

                                                    </tbody>
                                                </table>
                                            </div>
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                            <div class="col-xs-12 col-md-4">
                                <div class="card">
                                    <div class="card-body">            
                                            <h2>Microsoft</h2>
                                            <div>
                                                <table id="tblListaMicrosoft" class="table table-hover table-centered mb-0">
                                                    <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Plataforma</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tblListaMicrosoftBody">

                                                    </tbody>
                                                </table>
                                            </div>
                                    </div> <!-- end card body-->
                                </div> <!-- end card -->
                            </div><!-- end col-->
                        </div>
                        
                    </div>
                </div> <!-- container -->

            </div> <!-- content -->

            <!-- Footer Start -->
            <?php
            
                include "paginas/footer.php";
            
            ?>
            <!-- end Footer -->

        </div>

        <!-- ============================================================== -->
        <!-- End Page content -->
        <!-- ============================================================== -->


    </div>
    <!-- END wrapper -->


    <!-- Right Sidebar -->
    <?php
    
        include "paginas/right-sidebar.php";
    
    ?>
    <!-- /End-bar -->

    <!-- Template -->
    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/app.min.js"></script>
    <!-- Plugins -->
    <!-- -- Rating -->
    <script src="assets/js/vendor/jquery.rateit.min.js"></script>
    <!-- Checkbox -->
    <script src="assets/js/vendor/jquery.dataTables.min.js"></script>
    <script src="assets/js/vendor/dataTables.bootstrap5.js"></script>
    <script src="assets/js/vendor/dataTables.responsive.min.js"></script>
    <script src="assets/js/vendor/responsive.bootstrap5.min.js"></script>
    <script src="assets/js/vendor/dataTables.checkboxes.min.js"></script>

    <!-- Jquery -->
    <script src="assets_base/js/plugins/jquery/jquery.min.js"></script>
    
    <!-- Ajax -->
    <script src="assets_base/js/ajax/catalogo_videojuegos.ajax.js"></script>

    <!-- Custom -->
    <script src="assets_base/js/main.js"></script>
</body>

</html>