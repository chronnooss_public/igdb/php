// https://id.twitch.tv/oauth2/token?client_id=efbd78h8gkhku20i2assug435n3mpl&client_secret=tfuxhjahn52lwaue0wtriovtf5mnxu&grant_type=client_credentials
// client_id -> efbd78h8gkhku20i2assug435n3mpl
// client_secret -> tfuxhjahn52lwaue0wtriovtf5mnxu

const fetchData = async function () {
   const clientid = "efbd78h8gkhku20i2assug435n3mpl";
   const secretid = "l8azwi4togisr6s5s9ti7nfgsxsih4";
   const urlToken =
      "https://id.twitch.tv/oauth2/token?client_id=" +
      clientid +
      "&client_secret=" +
      secretid +
      "&grant_type=client_credentials";
   const requestOptionsTokenApi = {
      method: "POST",
      mode: "cors",
   };

   try {
      const res = await fetch(urlToken, requestOptionsTokenApi);
      const data = await res.json();
      return data;
   } catch (e) {
      console.log("error -> " + e);
   }
};

// async function obtData() {
//    var a = await fetchData();
//    console.log("fetchData-> " + a.access_token);
// }

async function obtCatalogoVideojuegos() {
   $("#formCatalogoVideojuegos").submit(async (e) => {
      e.preventDefault();

      var varToken = await fetchData();

      const postData = {
         function: "catalogoVideojuegos",
         search: $("#txtBusquedaJuego").val(),
         limit: $("#selLimite").val(),
         platform: $("#selPlataformas option:selected").val(),
         token: varToken.access_token,
      };

      const url = "controladores/videojuegos.controlador.php";

      $.post(url, postData, (response) => {
         $("#tblCatalogoVideojuegos tbody tr").remove();
         var t = "";

         var nintendoPlatformsArray = [
            4, 5, 18, 19, 20, 21, 22, 24, 33, 37, 41, 130, 137, 159,
         ];
         var sonyPlatformsArray = [7, 8, 9, 38, 46, 48, 167];
         var microsoftPlatformsArray = [11, 12, 49, 169];
         var bgNintendo = "bg-nintendo";
         var bgSony = "bg-sony";
         var bgMicrosoft = "bg-microsoft";

         $.each(JSON.parse(response), function (i, item) {
            var tr = "";
            tr = `
                        <tr>
                           <td>
                              <div class='form-check'>
                                 <input type='checkbox' class='form-check-input' id='customCheck2'>
                                 <label class='form-check-label' for='customCheck2'></label>
                              </div>
                           </td>

                           <td class='align-middle'>
                              <div class='row'>
                                 <img style='width: 100px;' class='rounded mx-auto d-block' src="${item.gameUrlCoverImage}">
                              </div>
                           </td>

                           <td class='align-middle'>${item.gameName}</td>
                     `;

            tr += `<td class='align-middle'>`;
            for (var x = 0; x < item.gamePlatform.length; x++) {
               tr += `<span class='badge ${item.gamePlatform[x]["classPlatform"]} mx-1'>${item.gamePlatform[x]["abbrevNamePlatform"]}</span>`;
            }
            tr += `</td>`;
            tr += `<td class='align-middle'>`;
            for (var x = 0; x < item.gameGenre.length; x++) {
               tr += `<span class='badge badge-warning-lighten mx-1'>${item.gameGenre[x]["nameGenre"]}</span>`;
            }
            tr += `</td>`;
            tr += `<td class='align-middle'>`;
            for (var x = 0; x < item.gameGameMode.length; x++) {
               tr += `<span class='badge badge-info-lighten mx-1'>${item.gameGameMode[x]["nameGameMode"]}</span>`;
            }
            tr += `</td>`;

            tr += `</tr>`;
            t += tr;
         });
         document.getElementById("tblCatalogoVideojuegosBody").innerHTML += t;
      });
   });
   return false;
}

async function obtListadoPlataformas() {
   var varToken = await fetchData();

   const listadoPlataformasData = {
      function: "listadoPlataformas",
      token: varToken.access_token,
   };

   const url = "controladores/videojuegos.controlador.php";
   $.post(url, listadoPlataformasData, (response) => {
      var codListaNintendo = "";
      var sumN = "";
      var codListaSony = "";
      var sumS = "";
      var codListaMicrosoft = "";
      var sumX = "";

      $("#tblListaNintendo tbody tr").remove();
      $("#tblListaSony tbody tr").remove();
      $("#tblListaMicrosoft tbody tr").remove();

      $.each(JSON.parse(response), function (i, item) {
         if (item.platformFamily == 5) {
            sumN += `<tr>
                        <td>${item.platformId}</td>
                        <td>${item.platformAbbrev}</td>
                     </tr>`;
         }

         if (item.platformFamily == 1) {
            sumS += `<tr>
                        <td>${item.platformId}</td>
                        <td>${item.platformAbbrev}</td>
                     </tr>`;
         }

         if (item.platformFamily == 2) {
            sumX += `<tr>
                        <td>${item.platformId}</td>
                        <td>${item.platformAbbrev}</td>
                     </tr>`;
         }
      });

      codListaNintendo += sumN;
      codListaSony += sumS;
      codListaMicrosoft += sumX;

      document.getElementById("tblListaNintendoBody").innerHTML +=
         codListaNintendo;
      document.getElementById("tblListaSonyBody").innerHTML += codListaSony;
      document.getElementById("tblListaMicrosoftBody").innerHTML +=
         codListaMicrosoft;
   });
   return false;
}

$(document).ready(function () {
   obtListadoPlataformas();
   obtCatalogoVideojuegos();
});
