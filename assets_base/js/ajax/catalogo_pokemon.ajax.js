$(document).ready(function () {
   obtPokedexPorGeneracion();
});

function obtPokedexPorGeneracion() {
   $("#formCatalogoPokemon").submit(async (e) => {
      e.preventDefault();

      let busqueda = $("#txtBusquedaPokemon").val();
      let generacion = $("#selGeneraciones option:selected").val();

      let min;
      let max;

      switch (generacion) {
         case "1":
            min = 1;
            max = 151;
            console.log("Generación 1");
            break;
         case "2":
            min = 152;
            max = 251;
            console.log("Generación 2");
            break;
         case "3":
            min = 252;
            max = 386;
            console.log("Generación 3");
            break;
         case "4":
            min = 387;
            max = 494;
            console.log("Generación 4");
            break;
         case "5":
            min = 495;
            max = 649;
            console.log("Generación 5");
            break;
         case "6":
            min = 650;
            max = 721;
            console.log("Generación 6");
            break;
         case "7":
            min = 722;
            max = 809;
            console.log("Generación 7");
            break;
         case "8":
            min = 810;
            max = 898;
            console.log("Generación 8");
            break;
         default:
            min = 1;
            max = 898;
      }

      const fetchData = async (busqueda, min, max) => {
         try {
            $("#tblPokedexGen tbody tr").remove();

            var t = "";
            if (busqueda == "") {
               for (var k = min; k <= max; k++) {
                  var tr = "";
                  const res = await fetch(
                     "https://pokeapi.co/api/v2/pokemon/" + k
                  );
                  const data = await res.json();

                  tr += `
                        <tr>
                           <td class='align-middle'>${data.id}</td>
                           <td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${k}.png></div></td>
                           <td class='align-middle'>${data.name}</td>
                       `;

                  tr += `  <td class='align-middle'>`;
                  $.each(data.types, function (i, item) {
                     tr += `  <span class='badge bg-${item.type.name} mx-1'>${item.type.name}</span>`;
                  });
                  tr += `  </td>`;

                  tr += `
                           <td class='align-middle'>${data.stats[0].base_stat}</td>
                           <td class='align-middle'>${data.stats[1].base_stat}</td>
                           <td class='align-middle'>${data.stats[2].base_stat}</td>
                           <td class='align-middle'>${data.stats[3].base_stat}</td>
                           <td class='align-middle'>${data.stats[4].base_stat}</td>
                           <td class='align-middle'>${data.stats[5].base_stat}</td>
                        </tr>
                       `;
                  t += tr;
               }
            } else {
               const res = await fetch(
                  "https://pokeapi.co/api/v2/pokemon/" + busqueda
               );
               const data = await res.json();
               var tr = "";
               tr += `
                        <tr>
                           <td class='align-middle'>${data.id}</td>
                           <td class='align-middle'> <div class='row'> <img style='width:100px;' class='rounded mx-auto d-block' src= https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${data.id}.png></div></td>
                           <td class='align-middle'>${data.name}</td>
                       `;

               tr += `  <td class='align-middle'>`;
               $.each(data.types, function (i, item) {
                  tr += `  <span class='badge bg-${item.type.name} mx-1'>${item.type.name}</span>`;
               });
               tr += `  </td>`;

               tr += `
                           <td class='align-middle'>${data.stats[0].base_stat}</td>
                           <td class='align-middle'>${data.stats[1].base_stat}</td>
                           <td class='align-middle'>${data.stats[2].base_stat}</td>
                           <td class='align-middle'>${data.stats[3].base_stat}</td>
                           <td class='align-middle'>${data.stats[4].base_stat}</td>
                           <td class='align-middle'>${data.stats[5].base_stat}</td>
                        </tr>
                       `;
               t += tr;
            }
            document.getElementById("tbltblPokedexGenBody").innerHTML += t;
         } catch (error) {
            console.log(error);
         }
      };

      fetchData(busqueda, min, max);
      return false;
   });
}
