<?php

    require_once "igdb/class.igdb.php";

    Class ModeloVideojuegos{

        static public function mdlMostrarCatalogoVideojuegos($fields, $search, $limit, $wPlatform, $token)
        {
            // $igdb = new IGDB("efbd78h8gkhku20i2assug435n3mpl", "84b2ofy7m9op7zs4ivhevrjvt16fgh"); 
            $igdb = new IGDB("efbd78h8gkhku20i2assug435n3mpl", $token); 
            
            $igdbUtil = new IGDBUtils();
            $builder = new IGDBQueryBuilder();
            
            try {

                if($wPlatform > 0)
                {
                    $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("platforms = (".$wPlatform.")")
                                            ->limit($limit)
                                            ->build()
                                        );
                }else{
                    $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            // ->where("platforms.platform_family = (1,2,5)")
                                            ->limit($limit)
                                            ->build()
                                        );
                }

                foreach ($result as $key => $value){
            
                    $gameId = "";
                    $gameCoverImage = "";
                    $gameName = "";
                    $gameGenres = array();
                    $gamePlatforms = array();
                    $gameGameModes = array();

                    // Id
                    $gameId = $value->id;
                    // Id de la imagen cover
                    if(isset($value->cover->image_id)){
                        $gameCoverImage  =  $value->cover->image_id;
                        $gameUrlCoverImage = IGDBUtils::image_url($value->cover->image_id, "cover_big");
                    }
                    // Nombre del juego
                    $gameName = $value->name;
                    // Generos
                    if(isset($value->genres))
                    {
                        foreach ($value->genres as $key2 => $value2) {
                            if(isset($value2->name) && $value2->name != null){
                                $gameGenres[] = array('nameGenre' => $value2->name);
                            }
                        }
                    }
                    // Plataformas
                    if(isset($value->platforms))
                    {
                        foreach ($value->platforms as $key2 => $value2) {

                            $arrayIdNintendoPlatforms = array(4, 5, 18, 19, 20, 21, 22, 24, 33, 37, 41, 130, 137, 159);
                            $arrayIdSonyPlatforms = array(7, 8, 9, 38, 46, 48, 167);
                            $arrayIdMicrosoftPlatforms = array(11, 12, 49, 169);
                            $idPlatform = $value2->id;
                            $classPlatform = "";

                            if (in_array($idPlatform, $arrayIdNintendoPlatforms))
                            {
                                // $classPlatform = "bg-nintendo";
                                 $classPlatform = "badge-danger-lighten";
                                // $classPlatform = "badge-outline-danger";
                            }elseif (in_array($idPlatform, $arrayIdSonyPlatforms))
                            {
                                // $classPlatform = "bg-sony";
                                 $classPlatform = "badge-primary-lighten";
                                // $classPlatform = "badge-outline-primary";
                            }elseif (in_array($idPlatform, $arrayIdMicrosoftPlatforms))
                            {
                                // $classPlatform = "bg-microsoft";
                                 $classPlatform = "badge-success-lighten";
                                // $classPlatform = "badge-outline-success";
                            }else{
                                // $classPlatform = "bg-general";
                                 $classPlatform = "badge-secondary-lighten";
                                //$classPlatform = "badge-outline-secondary";
                            }

                            if(isset($value2->name)){
                                if(isset($value2->abbreviation)){
                                    $gamePlatforms[] = array('idPlatform' => $value2->id, 'namePlatform' => $value2->name, 'abbrevNamePlatform' => $value2->abbreviation, 'classPlatform' => $classPlatform);
                                }else{
                                    $gamePlatforms[] = array('idPlatform' => $value2->id, 'namePlatform' => $value2->name, 'abbrevNamePlatform' => $value2->name, 'classPlatform' => $classPlatform);
                                }
                            }
                        }
                    }
                    // Generos
                    if(isset($value->game_modes))
                    {
                        foreach ($value->game_modes as $key2 => $value2) {
                            if(isset($value2->name) && $value2->name != null){
                                $gameGameModes[] = array('nameGameMode' => $value2->name);
                            }
                        }
                    }
                    $arrayGame[] = array('gameId' => $gameId, 'gameCoverImage' => $gameCoverImage, 'gameUrlCoverImage' => $gameUrlCoverImage, 'gameName' => $gameName, 'gameGenre' => $gameGenres, 'gamePlatform' => $gamePlatforms, 'gameGameMode' => $gameGameModes);
                }                    

                return $arrayGame;

            } catch (IGDBInvalidParameterException $e) {
                // invalid parameter passed to the builder
                echo $e->getMessage();
            } catch (IGDBEndpointException $e) {
                // failed query
                echo $e->getMessage();
            }
        }

        static public function MdlMostrarListadoPlataformas($fields, $limit, $token)
        {
            // $igdb = new IGDB("efbd78h8gkhku20i2assug435n3mpl", "84b2ofy7m9op7zs4ivhevrjvt16fgh"); 
            $igdb = new IGDB("efbd78h8gkhku20i2assug435n3mpl", $token); 

            
            $igdbUtil = new IGDBUtils();
            $builder = new IGDBQueryBuilder();
            
            try {
                $result = $igdb->platform(
                                        $builder
                                            ->fields($fields)
                                            ->where("id = (18,19,4,21,5,41,130,33,22,24,20,159,37,137,7,8,9,38,46,48,167,11,12,49,169)")
                                            ->limit($limit)
                                            ->sort("id asc")
                                            ->build()
                                    );

                foreach ($result as $key => $value){
            
                    $platformId = "";
                    $platformName = "";
                    $platformAbbrev = "";
                    $platformFamily = "";

                    // Id
                    $platformId = $value->id;
                    // Nombre de la plataforma
                    $platformName = $value->name;
                    // Nombre del juego
                    if(isset($value->abbreviation) && $value->abbreviation != null){
                        $platformAbbrev = $value->abbreviation;
                    }else{
                        $platformAbbrev = $value->name;
                    }
                    $platformFamily = $value->platform_family;
                    

                    $arrayListPlatform[] = array('platformId' => $platformId, 'platformName' => $platformName, 'platformAbbrev' => $platformAbbrev, 'platformFamily' => $platformFamily);
                }                    

                return $arrayListPlatform;

            } catch (IGDBInvalidParameterException $e) {
                // invalid parameter passed to the builder
                echo $e->getMessage();
            } catch (IGDBEndpointException $e) {
                // failed query
                echo $e->getMessage();
            }
        }
        
    } 
?>