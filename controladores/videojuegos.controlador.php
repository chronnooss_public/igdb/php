<?php

    require_once "modelos/videojuegos.modelo.php";

    if(isset( $_POST['function'] )){
        switch ($_POST['function']) {
            case "catalogoVideojuegos":
                //Campos
                $fields = "id, name, game_modes.*, artworks.*, screenshots.*, platforms.*, cover.*, platforms.platform_family.*, genres.*";
                //Busqueda
                $search = $_POST['search'];
                //Límite
                $limit = $_POST['limit'];
                //Plataforma
                $wPlatform = $_POST['platform'];

                $token = $_POST['token'];
                // echo "controlador-> ".$wPlatform;
                $resultadoBase = (array) ModeloVideojuegos::MdlMostrarCatalogoVideojuegos($fields, $search, $limit, $wPlatform, $token);
                echo json_encode($resultadoBase);
                return true;
                break;
            case "listadoPlataformas":
                //Campos
                $fields = "*";
                //Límite
                $limit = 100;

                $token = $_POST['token'];

                $resultadoBase = (array) ModeloVideojuegos::MdlMostrarListadoPlataformas($fields, $limit, $token);
                echo json_encode($resultadoBase);
                return true;
                break;
        }
    }

    
    
?>